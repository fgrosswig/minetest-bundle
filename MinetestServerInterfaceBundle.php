<?php

/*
 * This file is part of Minetest Server Interface.
 *
 * Copyright (c) 2017 King-Arthur's Team
 *
 * @license LGPL-3.0+
 */

namespace Minetest\ServerInterfaceBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the Minetest Server Interface bundle.
 *
 * @author King-Artbur's Team <https://minetest.king-arthur.eu>
 */
class MinetestServerInterfaceBundle extends Bundle
{
}
