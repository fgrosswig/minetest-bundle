<?php
/**
 * Created by JetBrains PhpStorm.
 * User: falk
 * Date: 27.08.13
 * Time: 23:06
 * To change this template use File | Settings | File Templates.
 */
/
namespace Minetest\ServerInterfaceBundle;

use Symfony\Component\HttpFoundation\Response;

/**
 * Description of TimelineAjax
 *
 * @author Johannes
 */
class APIopenlayerJSON extends \BackendModule

{

    public function checkLayer($varValue)
    {

        $objResult = $this->Database->prepare("SELECT COUNT(id) AS count FROM tl_mtvectors WHERE id =?")
            ->limit(1)
            ->execute($varValue);
        $erg = $objResult->count;
        if ($erg == 0) {
            //$objWidget->addError('The ' . $objWidget->label . ' is in use, not registered or not unlocked at this time".');
            return false;
        }
        return true;
    }


    protected $strTemplate = 'be_vectormapeditor';


    protected function compile() {
        {
            if (\Input::post('FORM_SUBMIT') == 'tl_mtvectoreditor')
            {
                if(isset($_POST['delete'])){
                    $layerid =  $this->Template->id = \Input::post('id');
                    $this->Database->prepare("DELETE FROM `tl_mtvectors` WHERE `id` = $layerid")
                        ->execute();
                }
                $this->Template->id = \Input::post('id');
                $this->Template->title = \Input::post('title');
                $this->Template->layer = \Input::post('layer');
                $this->Template->icon = \Input::post('icon');
                $this->Template->imgurl = \Input::post('imgurl');
                $this->Template->description = \Input::post('description');
                $this->Template->colour = \Input::post('colour');
                $this->Template->geometry = \Input::post('geometry');

                if(!isset($this->Template->id) || ($this->Template->id < 1)) {
                    $this->Template->newentry = "True";

                    $title = htmlspecialchars($this->Template->title);
                    $description = htmlspecialchars($this->Template->description);

                    $this->Database->prepare("INSERT INTO `tl_mtvectors` SET `tstamp` = UNIX_TIMESTAMP(),
                                                `title` = '".title."',
                                                `description` = '".$description."',
                                                `geometry` = '".$this->Template->geometry."',
                                                `imgurl` = '".$this->Template->imgurl."',
                                                `icon` = '".$this->Template->icon."',
                                                `colour` = '".$this->Template->colour."',
                                                `layer` = '".$this->Template->layer."'")
                        ->execute();
                } else {
                    $this->Template->newentry = "False";
                    if ($this->checkLayer($this->Template->id) == True) {

                        $title = htmlspecialchars($this->Template->title);
                        $description = htmlspecialchars($this->Template->description);

                        $this->Database->prepare("UPDATE `tl_mtvectors` SET `tstamp` = UNIX_TIMESTAMP(),
                                                `title` = '".$title."',
                                                `description` = '".$description."',
                                                `geometry` = '".$this->Template->geometry."',
                                                `imgurl` = '".$this->Template->imgurl."',
                                                `icon` = '".$this->Template->icon."',
                                                `colour` = '".$this->Template->colour."',
                                                `layer` = '".$this->Template->layer."'
                                                 WHERE id =?")
                            ->execute(intval($this->Template->id));
                    }
                }
            }

            $this->Template->JsonURLPlayerHouses = \Environment::get('url') . \Environment::get('path') . '/SimpleAjax.php?module=minetest&layer=playerhouses';
            $this->Template->JsonURLCities = \Environment::get('url') . \Environment::get('path') . '/SimpleAjax.php?module=minetest&layer=city';
            $this->Template->JsonURLPublicPlaces= \Environment::get('url') . \Environment::get('path') . '/SimpleAjax.php?module=minetest&layer=publicplaces';
            $this->Template->JsonURLPlayerLocation = \Environment::get('url') . \Environment::get('path') . '/SimpleAjax.php?module=minetest&layer=playerlocation';
        }
        $this->Template->title = "test";
        $this->Template->action = "elements/main.php?do=mteditorapi&submit&ref=".$this->Input->get('ref');

        $layername = $this->Database->prepare("SELECT * FROM tl_mtvectorlayers")->execute();

        while ($layername->next())
        {
            $newArr = array
            (
                'id' => $layername->id,
                'layername' => $layername->layername
            );
            $layeroption[] = $newArr;
        }

        $this->Template->layeroption = $layeroption;





    }



}

