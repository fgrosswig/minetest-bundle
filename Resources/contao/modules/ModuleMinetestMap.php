<?php


namespace Minetest\ServerInterfaceBundle;

class ModuleMinetestMap extends \Module
{
    protected $strTemplate = 'mod_map';
    private $versions = [];
    private $mapDirName = "map";
    private $currentMapDir = "map";

    public function compile()
    {
        $this->getVersions();
        $arrOptions = [
            'width'         => $this->map_imageWidth,
            'height'        => $this->map_imageHeight,
            'jsonApiUrl'    => $this->map_JSON_API_url,
            'markerUrl'     => $this->map_markerUrl,
            'mapUrl'        => $this->map_mapUrl . '/' . $this->mapDirName . "/"
        ];

        $this->Template->options = $arrOptions;
        $this->Template->versions = $this->versions;
    }

    private function getVersions(){
        $dir = TL_ROOT . '/' . $this->map_mapUrl;

        foreach(new \DirectoryIterator($dir) as $file){
            //print '<br>' . $file->getFilename();
            if(!$file->isDir() || $file->isDot()){
                continue;
            }
            $fileName = $file->getFilename();
            $date = $this->getDate($fileName);

            if(!$date){
                continue;
            }

            $this->versions[] = [
                'date'      => $date,
                'timestamp' => ($date=="Current")?time():strtotime($date),
                'name'      => $fileName,
                'url'       => $this->Environment->url . TL_PATH . '/' . $this->map_mapUrl . '/' . $fileName
            ];
        }

        usort($this->versions, function ($a, $b){
            if ($a['timestamp'] == $b['timestamp']) {
                return 0;
            }
            return ($a['timestamp'] < $b['timestamp']) ? 1 : -1;
        });
    }

    private function getDate($dirName){
        $arrDateTime = explode("-", $dirName);
        if(!$arrDateTime[0] == "map"){
            return false;
        }
        if($arrDateTime[1]==""){
            return "Current";
        }
        $day = substr($arrDateTime[1], 0, 2);
        $month = substr($arrDateTime[1], 2, 2);
        $year = substr($arrDateTime[1], 4, 4);

        return $day . "." . $month . "." . $year;
    }

}