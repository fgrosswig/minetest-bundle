<?php
/**
 * Created by JetBrains PhpStorm.
 * User: falk
 * Date: 29.08.13
 * Time: 20:17
 * To change this template use File | Settings | File Templates.
 */

class OpenLayer extends System {

    public function getJSON()
    {
        $objDatabase = \Database::getInstance();

        if($this->Input->get('module') == 'minetest')
        {

            if($this->Input->get('layer') == 'playerhouses' or 'city' or 'publicplaces' or 'geo') {

                $layer   = $this->Input->get('layer');
                $row2 = $objDatabase->prepare("SELECT
                                                    tl_mtvectors.id as id,
                                                    tl_mtvectors.title as title,
                                                    tl_mtvectors.description as description,
                                                    tl_mtvectors.layer as layer,
                                                    tl_mtvectors.icon as icon,
                                                    tl_mtvectors.colour as colour,
                                                    tl_mtvectors.imgurl as imgurl,
                                                    tl_mtvectors.geometry as geometry
                                               FROM tl_mtvectors LEFT JOIN tl_mtvectorlayers ON tl_mtvectors.layer=tl_mtvectorlayers.id WHERE `layername` LIKE ?")
                    ->execute('%'.$layer.'%');


                $sources = $row2->fetchAllAssoc();

                $output = array();
                $output['type'] = "FeatureCollection";
                $output['features'] = array();


                //$this->import('Database');


                $i = 0;
                foreach ($sources as $value) {

                    $output['features'][$i] = array();
                    $output['features'][$i]['type'] = "Feature";
                    $output['features'][$i]['properties'] = array();
                    $output['features'][$i]['properties']['title'] = $value["title"];
                    $output['features'][$i]['properties']['description'] = $value["description"];
                    $output['features'][$i]['properties']['id'] = $value["id"];
                    $output['features'][$i]['properties']['imgurl'] = $value["imgurl"];
                    $output['features'][$i]['properties']['layer'] = json_decode($value["layer"],true);
                    $output['features'][$i]['properties']['icon'] = $value["icon"];
                    $output['features'][$i]['properties']['colour'] = $value["colour"];
                    $output['features'][$i]['geometry'] = json_decode($value["geometry"],true);
                    $output['features'][$i]['crs'] = array();
                    $output['features'][$i]['crs']['type'] = "name";
                    $output['features'][$i]['crs']['properties'] = array();
                    $output['features'][$i]['crs']['properties']['name'] = "urn:ogc:def:crs:OGC:1.3:CRS84";

                    $i++;
                }

            }
            if($this->Input->get('layer') == 'playerlocation') {
                $layer   = $this->Input->get('layer');
                $row2 = $objDatabase->prepare("SELECT
                                                    `tl_mtusers`.`id`,
                                                    `playername`,
                                                    `cords` AS `position`,
                                                    `rotation`,
                                                    `death_count`,
                                                    `first_login`,
                                                    `last_login`,
                                                    `tl_mtgroup`.`id` AS `groupid`,
                                                    `tl_mtgroup`.`groupname`,
                                                    IF(`override_rights` = 0, `mt_privs`, `grouprights`) AS `privs`
                                                    FROM `tl_mtusers` 
                                                    LEFT JOIN `tl_mtgroup` ON `tl_mtusers`.`mtgroup` = `tl_mtgroup`.`id` 
                                                    WHERE `status` = 1;")
                    ->execute();


                $sources = $row2->fetchAllAssoc();

                $output = array();
                $output['type'] = "FeatureCollection";
                $output['features'] = array();

                $i = 0;
                foreach ($sources as $value) {
                    preg_match("/([-]?\d*),([-]?\d*),([-]?\d*)/", $value["position"], $match);
                    if(count($match) == 4) { // If $position have 4 elements

                        $playercord = array();
                        $playercord[] = (int)$match[1]+3000; // Coordinates starts at 1 in array
                        $playercord[] = (int)$match[3]-3000;

                        $output['features'][$i] = array();
                        $output['features'][$i]['type'] = "Feature";
                        $output['features'][$i]['geometry'] = array();
                        $output['features'][$i]['geometry']['type'] = "Point";
                        $output['features'][$i]['geometry']['coordinates'] = $playercord;
                        $output['features'][$i]['properties'] = $value;
                        $output['features'][$i]['properties']['layer'] = 5;
                        $output['features'][$i]['properties']['height'] = $match[2];
                        $i++;
                    }

                }
            }
            /*
            * Ausgabe des JSON-Files
            */
            header('Content-Type: application/json');
            echo json_encode($output);
            exit;
        }
    }
}