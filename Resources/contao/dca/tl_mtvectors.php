<?php


$GLOBALS['TL_DCA']['tl_mtvectors'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'enableVersioning'            => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        ),
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 2,
            'fields'                  => array('title','layer'),
            'flag'                    => 1,
            'panelLayout'             => 'search,sort,filter'
        ),
        'label' => array
        (
            'fields'                  => array('title','layer'),
            'format'                  => '%s'
        ),
        'global_operations' => array
        (
            /*'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            )*/
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_mtvectors']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'edit.gif',
                'attributes'		 => 'class="edit"',
            ),

            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_mtvectors']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
            'show' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_mtvectors']['show'],
                'href'                => 'act=show',
                'icon'                => 'show.gif',
                'attributes'          => 'style="margin-right:3px"'
            ),
        )
    ),

    // Palettes
    'palettes' => array
    (
        'default'                     => '{title_legend},title,imgurl,layer,description,icon,colour,geometry'
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtvectors']['title'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'search'                  => true,
            'eval'                    => array(
                'mandatory'=>true,
                'unique'=>true
            ),

            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'description' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtvectors']['description'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'sql'                     => "text NULL"
        ),
        'layer' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtvectors']['layer'],
            'inputType'               => 'select',
            'search'                  => true,
            'foreignKey'		     => 'tl_mtvectorlayers.layername',
            'sql'                     => "int(10) NOT NULL default '1'",
        ),
        'imgurl' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtvectors']['imgurl'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'sql'                     => "text NULL"
        ),
        'icon' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtvectors']['icon'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'colour' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtvectors']['colour'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'sql'                     => "varchar(255) NOT NULL default 'yellow'"
        ),
        'geometry' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtvectors']['geometry'],
            'inputType'               => 'textarea',
            'exclude'                 => false,
            'flag'                    => 1,
            'eval'                    => array(
                'mandatory'=>true
            ),

            'sql'                => "text NULL"
        )
    )
);
