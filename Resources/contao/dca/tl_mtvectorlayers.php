<?php


$GLOBALS['TL_DCA']['tl_mtvectorlayers'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'enableVersioning'            => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
                'layername' => 'unique'
            )
        ),
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 2,
            'fields'                  => array('layername'),
            'flag'                    => 1,
            'panelLayout'             => 'filter;search,sort,limit'
        ),
        'label' => array
        (
            'fields'                  => array('layername'),
            'format'                  => '%s'
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_mtvectorlayers']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'edit.gif'
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_mtvectorlayers']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
            'show' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_mtvectorlayers']['show'],
                'href'                => 'act=show',
                'icon'                => 'show.gif',
                'attributes'          => 'style="margin-right:3px"'
            ),
        )
    ),

    // Palettes
    'palettes' => array
    (
        'default'                     => '{layer_legend},layername,layerdescription' //,test'
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(11) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'layername' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtvectorlayers']['layername'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'search'                  => true,
            'eval'                    => array(
                'mandatory'=>true,
                'unique'=>true,
                'maxlength'=>255,
            ),

            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'layerdescription' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtvectorlayers']['layerdescription'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>true,
                'style'=>'height:40px',
                'maxlength'=>255,
            ),
            'sql'                     => "varchar(255) NOT NULL default ''"
        )
    )
);



