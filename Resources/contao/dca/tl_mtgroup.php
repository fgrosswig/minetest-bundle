<?php


$GLOBALS['TL_DCA']['tl_mtgroup'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'enableVersioning'            => true,
        //'ctable'			  => array('tl_minetest'),
        //'switchToEdit'		  => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        ),
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 2,
            'fields'                  => array('groupname'),
            'flag'                    => 1,
            'panelLayout'             => 'filter;search,sort,limit'
        ),
        'label' => array
        (
            'fields'                  => array('groupname'),
            'format'                  => '%s'
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_mtgroup']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'edit.gif'
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_mtgroup']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
            'show' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_mtgroup']['show'],
                'href'                => 'act=show',
                'icon'                => 'show.gif',
                'attributes'          => 'style="margin-right:3px"'
            ),
        )
    ),

    // Palettes
    'palettes' => array
    (
        'default'                     => '{groupname_legend},groupname,groupdescription,grouprights' //,test'
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(11) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'groupname' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtgroup']['groupname'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'search'                  => true,
            'eval'                    => array(
                'mandatory'=>true,
                'unique'=>true,
                'maxlength'=>255,
            ),

            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'groupdescription' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtgroup']['groupdescription'],
            'inputType'               => 'textarea',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>true,
                'style'=>'height:40px',
            ),
            'sql'                     => "text NULL",
        ),
        'grouprights' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtgroup']['grouprights'],
            'inputType'               => 'textarea',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>true,
                'style'=>'height:40px',
                'maxlength'=>255,
            ),
            'sql'                     => "varchar(255) NOT NULL default ''"
        )
        /*,

        'test' => array
        (
            'label'               => &$GLOBALS['TL_LANG']['tl_mtgroup']['test'],
                    'inputType'           => 'select',
                    'search'              => false,
                    'options'		 => array(),
                    'eval'                => array('mandatory'=>true, 'maxlength'=>255, 'submitOnChange'=>true),
                    'sql'                 => "varchar(255) NOT NULL default ''",
             )
        */
    )
);



