<?php

$GLOBALS['TL_DCA']['tl_mtusers'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'enableVersioning'            => true,
        //'ctable'			  => array('tl_minetest'),
        //'switchToEdit'		  => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        ),
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 2,
            'fields'                  => array('playername','mtgroup','last_login'),
            'flag'                    => 1,
            'panelLayout'             => 'filter;search,sort,limit'
        ),
        'label' => array
        (
            'fields'                  => array('playername','mtgroup','last_login'),
            'format'                  => '%s'
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_mtusers']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'edit.gif'
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_mtusers']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
            'show' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_mtusers']['show'],
                'href'                => 'act=show',
                'icon'                => 'show.gif',
                'attributes'          => 'style="margin-right:3px"'
            ),
        )
    ),

    // Palettes
    'palettes' => array
    (
        'default'                     => '{playername_legend},playername,password;{playerstats_legend},first_login,last_login,last_ip,status,cords,death_count;{rules_legend},accepted_rules;{rights_legend},mtgroup,override_rights,authorized,mt_privs'
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(11) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'playername' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['playername'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'search'                  => true,
            'eval'                    => array(
                'mandatory'=>true,
                'unique'=>true,
                'maxlength'=>255,
                'tl_class'=>'w50'
            ),

            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'password' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['password'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>true,
                'maxlength'=>400,
                'tl_class'=>'w50'
            ),
            'sql'                     => "varchar(400) NOT NULL default ''"
        ),
        'accepted_rules' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['accepted_rules'],
            'inputType'               => 'checkbox',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>false,
                'maxlength'=>1,
                'tl_class'=>'w25'
            ),
            'sql'                     => "tinyint(1) NOT NULL default '0'",
        ),

        'mt_privs' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['mt_privs'],
            'inputType'               => 'textarea',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>false,
                'style'=>'height:40px',
                'tl_class'=>'w50'
            ),
            'sql'                     => "text NULL",
        ),
        'status' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['status'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>false,
                'maxlength'=>1,
                'tl_class'=>'w50'
            ),
            'sql'                     => "tinyint(1) NOT NULL default '0'",
        ),
        'cords' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['cords'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>true,
                'maxlength'=>255,
                'tl_class'=>'w50'
            ),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),

        'override_rights' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['override_rights'],
            'inputType'               => 'checkbox',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>false,
                'maxlength'=>1,
                'tl_class'=>'w25'
            ),
            'sql'                     => "tinyint(1) NOT NULL default '1'",
        ),
        'authorized' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['authorized'],
            'inputType'               => 'checkbox',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>false,
                'maxlength'=>1,
                'tl_class'=>'w25'
            ),
            'sql'                     => "tinyint(1) NOT NULL default '0'",
        ),
        'first_login' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['first_login'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>false,
                'tl_class'=>'w50'
            ),
            'sql'                     => "datetime NULL",
        ),
        'last_login' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['last_login'],
            'inputType'               => 'text',
            'search'                  => true,
            'filter'		     => true,
            'eval'                    => array(
                'mandatory'=>false,
                'tl_class'=>'w50'
            ),
            'sql'                     => "datetime NULL",
        ),
        'last_logout' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['last_logout'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>false,
                'maxlength'=>1,
                'tl_class'=>'w50'
            ),
            'sql'                     => "datetime   NULL",
        ),
        'last_ip' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['last_ip'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>false,
                'maxlength'=>255,
                'tl_class'=>'w50'
            ),
            'sql'                     => "varchar(18) NOT NULL default ''"
        ),

        'mtgroup' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['mtgroup'],
            'inputType'               => 'select',
            'search'                  => true,
            'foreignKey'		     => 'tl_mtgroup.groupname',
            'sql'                     => "int(10) NOT NULL default '1'",
        ),
        'death_count' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['death_count'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>false,
                'tl_class'=>'w50'
            ),
            'sql'                     => "int(10) NOT NULL default '0'",
        ),
        'rotation' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['rotation'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>false,
                'tl_class'=>'w50'
            ),
            'sql'                     => "float",
        ),
        'exit_state' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_mtusers']['exit_state'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>false,
                'tl_class'=>'w50'
            ),
            'sql'                     => "enum( 'none', 'normal', 'timedout' ) NOT NULL default 'none'",
        )
    )
);



