<?php


$GLOBALS['TL_DCA']['tl_minetest'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        //'ptable'			          => tl_mtusers,
        'enableVersioning'            => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
                'serverport' => 'unique'
            )
        ),
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 0,
            'fields'                  => array('title'),
            'flag'                    => 1,
            'panelLayout'             => 'search,sort,filter'
        ),
        'label' => array
        (
            'fields'                  => array('title'),
            'format'                  => '%s'
        ),
        'global_operations' => array
        (
            /*'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            )*/
        ),
        'operations' => array
        (
            'edit-users' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_minetest']['edit-users'],
                'href'                => 'table=tl_mtusers',
                'icon'                => 'user.png',
                'attributes'		 => 'class="contextmenu"',
            ),
            'edit-vector' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_minetest']['edit-vector'],
                'href'                => 'table=tl_mtvectors',
                'icon'                => 'vmap.png',
                'attributes'		 => 'class="contextmenu"',
            ),
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_minetest']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'header.gif',
                'attributes'		 => 'class="edit"',
            ),

            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_minetest']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
            'show' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_minetest']['show'],
                'href'                => 'act=show',
                'icon'                => 'show.gif',
                'attributes'          => 'style="margin-right:3px"'
            ),
        )
    ),

    // Palettes
    'palettes' => array
    (
        'default'                     => '{title_legend},title,{servername_legend},servername;{mthostname},mthostname,serverport;{pid_dir_legend},pid_dir,log_dir,top_mtuser,mod_dir,mt_restart_script;{rules_legend},connect_rules,accept_rules_button;{messages_legend},after_registration,no_interact_message,with_interact_message,ondie_message,onrespawn_message'
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['title'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'search'                  => true,
            'eval'                    => array(
                'mandatory'=>true,
                'unique'=>true,
                'maxlength'=>255,
                'tl_class'=>'w50'
            ),

            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'servername' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['servername'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'sql'                     => "text NULL"
        ),
        'serverport' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['serverport'],
            'inputType'               => 'text',
            'exclude'                 => false,
            'flag'                    => 1,
            'eval'                    => array(
                'mandatory'=>true,
                'unique'=>true,
                'maxlength'=>5,
            ),

            'sql'                => "int(5) unsigned NOT NULL default '0'"
        ),

        'pid_dir' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['pid_dir'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>true,
                'unique'=>true,
                'maxlength'=>255,
                'tl_class'=>'w50'
            ),
            'sql'                     => "varchar(255) NOT NULL default ''",
        ),
        'log_dir' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['log_dir'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>true,
                'unique'=>true,
                'maxlength'=>255,
                'tl_class'=>'w50'
            ),
            'sql'                     => "varchar(255) NOT NULL default ''",
        ),
        'log_level' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['log_dir'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>true,
                'unique'=>true,
                'maxlength'=>1,
                'tl_class'=>'w50'
            ),
            'sql'                     => "int(10) NOT NULL default '1'",
        ),
        'top_mtuser' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['top_mtuser'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>true,
                'unique'=>true,
                'maxlength'=>255,
                'tl_class'=>'w50'
            ),
            'sql'                     => "varchar(255) NOT NULL default ''",
        ),
        'mod_dir' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['mod_dir'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>true,
                'unique'=>true,
                'maxlength'=>255,
                'tl_class'=>'w50'
            ),
            'sql'                     => "varchar(255) NOT NULL default ''",
        ),

        'mthostname' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['mthostname'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>true,
                'unique'=>false,
                'maxlength'=>255,
                'tl_class'=>'w50'
            ),
            'sql'                     => "varchar(255) NOT NULL default ''",
        ),
        'mt_restart_script' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['mt_restart_script'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'                    => array(
                'mandatory'=>true,
                'unique'=>true,
                'maxlength'=>255,
                'tl_class'=>'w50'
            ),
            'sql'                     => "varchar(255) NOT NULL default ''",
        ),
        'connect_rules' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['connect_rules'],
            'inputType'               => 'textarea',
            'exclude'                 => true,
            'eval'			     => array(
                'mandatory'=>true,
                'style'=>'width: 100%;height:400px',
            ),
            'sql'                     => "text NULL"
        ),
        'accept_rules_button' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['accept_rules_button'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'			     => array(
                'mandatory'=>true,
                'style'=>'width: 100%',
            ),
            'sql'                     => "text NULL"
        ),
        'no_interact_message' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['no_interact_message'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'			     => array(
                'mandatory'=>true,
                'style'=>'width: 100%;',
            ),
            'sql'                     => "text NULL"
        ),
        'with_interact_message' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['with_interact_message'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'			     => array(
                'mandatory'=>true,
                'style'=>'width: 100%;',
            ),
            'sql'                     => "text NULL"
        ),
        'ondie_message' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['ondie_message'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'			     => array(
                'mandatory'=>true,
                'style'=>'width: 100%;',
            ),
            'sql'                     => "text NULL"
        ),
        'after_registration' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['after_registration'],
            'inputType'               => 'textarea',
            'exclude'                 => true,
            'eval'			     => array(
                'mandatory'=>true,
                'style'=>'width: 100%;height:60px',
            ),
            'sql'                     => "text NULL"
        ),
        'onrespawn_message' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_minetest']['onrespawn_message'],
            'inputType'               => 'text',
            'exclude'                 => true,
            'eval'			     => array(
                'mandatory'=>true,
                'style'=>'width: 100%;',
            ),
            'sql'                     => "text NULL"
        ),

    )
);
