<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
    'Minetest'
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
    // Classes

    // Modules

));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
    /*

    'mod_sideNav'            => 'system/modules/minetest/templates',
    'mod_mtserver_moddir'    => 'system/modules/minetest/templates',
    'mod_mtserver_control'   => 'system/modules/minetest/templates',
    'mod_mtserver_overview'  => 'system/modules/minetest/templates',
    'mod_onlineplayers'      => 'system/modules/minetest/templates',
    'mod_minetest_list'      => 'system/modules/minetest/templates',
    'mod_mtserver_readlog'   => 'system/modules/minetest/templates',
    'mod_minetest_overview'  => 'system/modules/minetest/templates',
    'mod_minetest_user_list' => 'system/modules/minetest/templates',
    'mod_mtserver_top'       => 'system/modules/minetest/templates',
    'mod_navigation_tabs'    => 'system/modules/minetest/templates',
    'mod_mtserver_dellog'    => 'system/modules/minetest/templates',
    'mod_mtserver_logdir'    => 'system/modules/minetest/templates',
    'mtuser_detail'          => 'system/modules/minetest/templates',
    'mod_map'          => 'system/modules/minetest/templates',

    */

    'minetest_fepage' => 'templates'
));
