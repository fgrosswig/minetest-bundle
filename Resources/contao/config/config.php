<?php

/*
 * This file is part of Minetest Server Interface.
 *
 * Copyright (c) 2017 King-Arthur's Team
 *
 * @license LGPL-3.0+
 */

use Contao\CoreBundle\ContaoCoreBundle;

/**
 * Back end modules

array_insert($GLOBALS['BE_MOD']['Minetest Server'], 1 ,[
    'Core Server Settings' => [
        'tables' => ['tl_car'],

         *
         * $GLOBALS['TL_CSS'][] = 'system/modules/moduledir/assets/css/???.css';
         *
         * https://github.com/contao/core-bundle/blob/master/src/Resources/contao/classes/Backend.php#L291-L350
         *
         old style:
        'icon' => 'mt_1.jpeg',

         icons will be defined as background
         new style:

        the group name will be ported from $GLOBALS['BE_MOD']['Minetest Server']

        .group-Minetest Server {
	        background: url(../images/icon-location.png) 7px 10px no-repeat;
        }

        //'stylesheet' => 'assets/css/.css',


        'tables' => ['tl_minetest','tl_mtusers','tl_mtvectors'],
        'list' => ['ListWizard', 'importList']
    ]
]);
 */

//$GLOBALS['BE_MOD']['minetest']['mtusers'] = array (
//      'tables' 	=> array('tl_mtusers'),
//      'icon' 	    => 'system/modules/minetest/assets/images/user.png'
//      );
//$GLOBALS['TL_HOOKS']['simpleAjax'][] = array('OpenLayer','getJSON');


array_insert($GLOBALS['BE_MOD']['Minetest'], 1,  array
(
    // Minetest Server modules

    'Settings' => array
    (
        'tables' => ['tl_minetest','tl_mtusers','tl_mtvectors'],
        'table'       => array('TableWizard', 'importTable'),
        'list'        => array('ListWizard', 'importList')
    ),
    'Users' => array
    (
        'tables' 	=> array('tl_mtusers'),
        'table'       => array('TableWizard', 'importTable'),
        'list'        => array('ListWizard', 'importList')
    ),
    'Groups' => array
    (
        'tables' 	=> array('tl_mtgroup'),
        'table'       => array('TableWizard', 'importTable'),
        'list'        => array('ListWizard', 'importList')
    ),
    'Rights' => array
    (
        'tables' 	=> array('tl_mtrights'),
        'table'       => array('TableWizard', 'importTable'),
        'list'        => array('ListWizard', 'importList')
    ),
    'Map Layer' => array
    (
        'tables' 	=> array('tl_mtvectorlayers'),
        'table'       => array('TableWizard', 'importTable'),
        'list'        => array('ListWizard', 'importList')
    ),
    'Map Editor API' => array
    (
        // register backend MAP Editor
    )
));
