<?php

/*
 * This file is part of Minetest Server Interface.
 *
 * Copyright (c) 2017 King-Arthur's Team
 *
 * @license LGPL-3.0+
 */

namespace Minetest\ServerInterfaceBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
/*
 * use Symfony\Component\DependencyInjection\Loader\Twig; ???
 */

/**
 * Configures the Minetest Server Interface bundle.
 *
 * @author King-Artbur's Team <https://minetest.king-arthur.eu>
 */

class MinetestServerInterfaceExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $mergedConfig, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load('listener.yml');
    }
}
