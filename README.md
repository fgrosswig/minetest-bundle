Contao 4 Minetest Server Interface bundle
=========================================

assume you are in the root directory of contao you'll have 
to perform the following commands prior using it:

```
mkdir -p src/ && mkdir src/Minetest && mkdir src/Minetest/ServerInterfaceBundle
cd src/Minetest/ServerInterfaceBundle
```
```
git clone https://gitlab.king-arthur.eu/support/minetest-bundle.git .
```

you should see the following structure:


```
src
└── Minetest
    └── ServerInterfaceBundle
        ├── ContaoManager
        │   └── Plugin.php
        ├── DependencyInjection
        │   └── MinetestServerInterfaceExtension.php
        ├── EventListener
        │   └── FileMetaInformationListener.php
        ├── Resources
        │   ├── config
        │   │   └── listener.yml
        │   └── contao
        │       ├── classes
        │       │   └── MinetestVectorEditor.php
        │       ├── config
        │       │   ├── config.php
        │       │   └── ide_compat.php
        │       ├── dca
        │       │   ├── tl_minetest.php
        │       │   ├── tl_mtgroup.php
        │       │   ├── tl_mtrights.php
        │       │   ├── tl_mtusers.php
        │       │   ├── tl_mtvectorlayers.php
        │       │   └── tl_mtvectors.php
        │       ├── languages
        │       │   ├── de
        │       │   │   ├── default.xlf
        │       │   │   ├── modules.xlf
        │       │   │   └── tl_layout.xlf
        │       │   └── en
        │       │       ├── default.xlf
        │       │       ├── modules.xlf
        │       │       └── tl_layout.xlf
        │       └── modules
        │           ├── APIopenlayerJSON.php
        │           ├── ModuleMinetestMap.php
        │           ├── SimpleAjax.php
        │           └── json.php
        ├── MinetestServerInterfaceBundle.php
        ├── README.md
        ├── appveyor.yml
        └── phpunit.xml.dist
```

adding infos to composer.json

```
 "autoload": {
       "psr-0": {
            "": "src/"
        },
```

adding app/AppKernel.php

```
 public function registerBundles()
    {
        $bundles = [
	     ...,	     
	     new Minetest\ServerInterfaceBundle\MinetestServerInterfaceBundle(),	
	     ...

```

edit app/config/config.yml for doctrine hook:

```
doctrine:
    dbal:
        default_connection: default
        connections:
            default:
                driver: pdo_mysql
                host: "%database_host%"
                port: "%database_port%"
                user: "%database_user%"
                password: "%database_password%"
                dbname: "%database_name%"
                charset: UTF8
                mapping_types:
                    enum: string
```
