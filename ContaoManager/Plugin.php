<?php

/*
 * This file is part of Minetest Server Interface.
 *
 * Copyright (c) 2017 King-Arthur's Team
 *
 * @license LGPL-3.0+
 */

namespace ServerInterfaceBundle\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;

/**
 * Configures the Minetest Server Interface bundle.
 *
 * @author Leo Feyer <https://minetest.king-arthur.eu>
 */
class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create('Minetest\ServerInterfaceBundle\MinetestServerInterfaceBundle')
                ->setLoadAfter(['Contao\CoreBundle\ContaoCoreBundle'])
                ->setReplace(['minetest']),
        ];
    }
}
